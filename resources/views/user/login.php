<?php

use Yiisoft\Form\Widget\Form;
use Yiisoft\Html\Html;

$formOptions = ['csrf' => $csrf];
?>

<div class="container">
    <div class="row justify-content-center text-center">
        <div class="col col-4">
            <?php if ($error): ?>
                <div class="text-danger">
                    <?= $error ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col col-md-12 col-sm-12">
            <?= Form::widget()->action($urlGenerator->generate('login'))->options($formOptions)->begin() ?>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($form, 'email', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\TextInput::widget()->config($form, 'email', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($form, 'password', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\PasswordInput::widget()->config($form, 'password', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-success']) ?>
            </div>
            <?= Form::end() ?>
        </div>
    </div>
</div>
