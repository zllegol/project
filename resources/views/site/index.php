<?php

declare(strict_types=1);

/** @var App\ApplicationParameters $applicationParameters */

$this->params['breadcrumbs'] = '/';

$this->setTitle($applicationParameters->getName());
?>

<div class="container">
    <div class="row mt-4">
        <h3 class="title">Обзор</h3>
    </div>

    <div class="row mt-2">
        <p class="subtitle">
            Всего записей в таблице shipments: <b><?= $shipment_count ?></b>
        </p>
        <p class="subtitle">
            Всего записей в таблице invoices: <b><?= $invoice_count ?></b>
        </p>
    </div>
</div>
