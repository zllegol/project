<?php

declare(strict_types=1);

use App\Asset\AppAsset;
use App\Asset\CdnFontAwesomeAsset;
use Yiisoft\Html\Html;
use Yiisoft\I18n\Locale;
use Yiisoft\Yii\Bootstrap5\Nav;

/**
 * @var App\ApplicationParameters $applicationParameters
 * @var Yiisoft\Assets\AssetManager $assetManager
 * @var string $content
 * @var string|null $csrf
 * @var Locale $locale
 * @var Yiisoft\View\WebView $this
 * @var Yiisoft\Router\CurrentRoute $currentRoute
 * @var \Yiisoft\User\CurrentUser $currentUser
 */

$assetManager->register([
    AppAsset::class,
    CdnFontAwesomeAsset::class,
]);

$this->addCssFiles($assetManager->getCssFiles());
$this->addCssStrings($assetManager->getCssStrings());
$this->addJsFiles($assetManager->getJsFiles());
$this->addJsStrings($assetManager->getJsStrings());
$this->addJsVars($assetManager->getJsVars());
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Html::encode($locale->language()) ?>">
    <head>
        <meta charset="<?= Html::encode($applicationParameters->getCharset()) ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->getTitle()) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
            <header>
                <?php
                    if ($currentUser->isGuest()) {
                        $menuItems = [];
                    } else {
                        $menuItems = [
                            [
                                'label' => 'Обзор',
                                'url' => $urlGenerator->generate('home'),
                            ],
                            [
                                'label' => 'Перевозки',
                                'url' => $urlGenerator->generate('shipments'),
                            ],
                            [
                                'label' => 'Счета',
                                'url' => $urlGenerator->generate('invoices'),
                            ]
                        ];
                    }
                ?>
                <nav class="navbar navbar-dark bg-dark">
                    <div class="container">
                        <a class="navbar-brand" href="/">my.Forward</a>
                        <?php
                            echo Nav::widget()
                                ->currentPath($currentRoute->getUri()->getPath())
                                ->items($menuItems)
                                ->options([
                                    'class' => 'navbar-nav me-auto mb-2 mb-lg-0'
                                ]);
                        ?>
                        <?php if (!$currentUser->isGuest()): ?>
                            <div class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><?= $currentUser->getIdentity()->getEmail(); ?></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?= $urlGenerator->generate('logout'); ?>">Выйти</a></li>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </nav>
            </header>
            <main>
                <div class="container">
                    <?= $content ?>
                </div>
            </main>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
