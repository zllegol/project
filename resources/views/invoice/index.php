<?php
/* @var \App\Invoices\Form\InvoicesFilterForm $filterForm */
/** @var array $filterOptions */
/** @var array $gridData */
/* @var string $csrf */
/* @var \Yiisoft\Router\UrlGeneratorInterface $urlGenerator */

/** @var \Yiisoft\Data\Paginator\OffsetPaginator $paginator */

use Yiisoft\Form\Widget\Form;
use Yiisoft\Html\Html;

$paginator = $gridData['paginator'];

$formOptions = ['csrf' => $csrf];

foreach ($filterOptions as $key => $value) {
    if ($filterForm->hasAttribute($key)) {
        $filterForm->setAttribute($key, $value);
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col col-md-12 col-sm-12">
            <?= Form::widget()->action($urlGenerator->generate('invoices'))->options($formOptions)->begin() ?>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'name', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\TextArea::widget()->config($filterForm, 'name', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'doc_date_from', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'doc_date_from', ['class' => 'form-control']) ?>
                <span>&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'doc_date_to', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'due_date_from', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'due_date_from', ['class' => 'form-control']) ?>
                <span>&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'due_date_to', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'status', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DropDownList::widget()->config($filterForm, 'status', ['class' => 'form-select'])->items(['' => '-'] + $filterOptions['status_select_options']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Очистить', $urlGenerator->generate('invoices', ['filter_clear' => 1]), ['class' => 'btn btn-danger']) ?>
            </div>
            <?= Form::end() ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Счет №
                    </th>
                    <th>
                        Статус
                    </th>
                    <th>
                        Сумма
                    </th>
                    <th>
                        Дата документа
                    </th>
                    <th>
                        Срок оплаты
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($paginator->read() as $item): ?>
                    <?php /** @var \App\Entity\InvoiceEntity $item */ ?>
                    <tr>
                        <td>
                            <?= Html::a(
                                $item->getName() ? $item->getName() : 'ДЕТАЛИ',
                                $urlGenerator->generate('invoices.detail', ['id' => $item->getInvoiceId()])
                            ) ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getStatus() == 11 ? 'Не оплачен' : 'Оплачен'); ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getAmount() . ' ' . $item->getCurrency()) ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getDocDate() ? $item->getDocDate()->format('d.m.Y') : '-'); ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getDueDate() ? $item->getDueDate()->format('d.m.Y') : '-'); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <?= \App\Widget\OffsetPagination::widget()->paginator($paginator)->urlGenerator(fn($page) => $urlGenerator->generate('invoices', ['page' => $page])) ?>
    </div>
</div>

