<?php
/** @var \App\Entity\InvoiceEntity $item */

use Yiisoft\Html\Html;

?>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= $urlGenerator->generate('invoices') ?>">Счета</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?= Html::encode($item->getName() ? $item->getName() : 'Детали') ?></li>
        </ol>
    </nav>

    <div class="jow justify-content-start text-left mt-5 mb-3">
        <h5 class="text-left">Общая информация</h5>
    </div>
    <div class="row justify-content-start">
        <div class="col-5">
            <table class="table">
                <tbody>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>Счет №</td>
                    <td><?= Html::encode($item->getName()) ?></td>
                </tr>
                <tr>
                    <td>Клиент</td>
                    <td><?= Html::encode((($item->getCustomer() instanceof \App\Entity\CustomerEntity) ? $item->getCustomer()->getName() : '-')) ?></td>
                </tr>
                <tr>
                    <td>Юр. лицо клиента</td>
                    <td><?= Html::encode(($item->getCustomerAccount() instanceof \App\Entity\CustomerAccountEntity)? $item->getCustomerAccount()->getName() : '-') ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-1"></div>
        <div class="col-5">
            <table class="table">
                <tbody>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>Стаутс</td>
                    <td>
                        <?php if ($item->getStatus() == 22): ?>
                            <span class="btn-sm btn-success">Оплачен</span>
                        <?php else: ?>
                            Не оплачен
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>Сумма</td>
                    <td><b><?= Html::encode($item->getAmount() . ' ' . $item->getCurrency()) ?></b></td>
                </tr>
                <tr>
                    <td>Дата документа</td>
                    <td><?= Html::encode($item->getDocDate() ? $item->getDocDate()->format('d.m.Y') : '-'); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="jow justify-content-start text-left mt-5 mb-3">
        <h5 class="text-left">Услуги</h5>
    </div>

    <div class="row justify-content-start">
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Контейнер №</th>
                    <th>Описание</th>
                    <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $sum = 0;
                    $currency = '';
                ?>
                <?php foreach ($item->getInvoiceLines() as $invoiceLine): ?>
                    <?php
                        /** @var \App\Entity\InvoiceLineEntity $invoiceLine */

                        $sum += $invoiceLine->getAmount();
                        if (!$currency) {
                            $currency = $invoiceLine->getInvoice()->getCurrency();
                        }
                    ?>
                    <tr>
                        <td>
                            <?php if ($invoiceLine->getShipment()): ?>
                                <?= Html::a(
                                    $invoiceLine->getShipment()->getContainerName(),
                                    $urlGenerator->generate('shipments.detail', ['id' => $invoiceLine->getShipment()->getShipmentId()])
                                ) ?>
                            <?php else: ?>
                                -
                            <?php endif; ?>
                        </td>
                        <td style="width: 50%;">
                            <?= Html::encode($invoiceLine->getName() ?: '-') ?>
                        </td>
                        <td>
                            <?= Html::encode($invoiceLine->getAmount() . ' ' . ($invoiceLine->getInvoice() ? $invoiceLine->getInvoice()->getCurrency() : '')) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <b>Итого</b>&nbsp;&nbsp;&nbsp;
                        <b><?= Html::encode($sum . ' ' . $currency); ?></b>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
