<?php
/** @var \App\Entity\ShipmentEntity $item */

use Yiisoft\Html\Html;
?>

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= $urlGenerator->generate('shipments') ?>">Перевозки</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?= Html::encode($item->getContainerName() ? $item->getContainerName() : 'Детали') ?></li>
        </ol>
    </nav>

    <div class="row justify-content-start mt-5 mb-3">
        <div class="col-5">
            <h5 class="mb-3">Общая информация</h5>
            <table class="table">
                <tbody>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>
                        Контейнер №
                    </td>
                    <td>
                        <b><?= Html::encode($item->getContainerName()) ?></b>
                        &nbsp;
                        &nbsp;
                        <?= Html::encode($item->getContainerTypeId() ? $item->getContainerType()->getName() : '') ?>
                    </td>
                </tr>
                <tr>
                    <td>Номер пломбы</td>
                    <td><?= Html::encode($item->getSealNumber() ? $item->getSealNumber() : '-') ?></td>
                </tr>
                <tr>
                    <td>Менеджер по обслуживанию клиентов</td>
                    <td>-</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row justify-content-start mt-5">
        <div class="col-5">
            <h5 class="mb-3">Морская перевозка</h5>
            <table class="table">
                <tbody>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>
                        Коносамент
                    </td>
                    <td>
                        <b><?= Html::encode($item->getContainerName()) ?></b>
                        &nbsp;
                        &nbsp;
                        <?= Html::encode($item->getContainerTypeId() ? $item->getContainerType()->getName() : '') ?>
                    </td>
                </tr>
                <tr>
                    <td>Морская линия</td>
                    <td><?= Html::encode($item->getSealNumber() ? $item->getSealNumber() : '-') ?></td>
                </tr>
                <tr>
                    <td>Порт погрузки</td>
                    <td><?= Html::encode($item->getPol() ? $item->getPol()->getName() : '-') ?></td>
                </tr>
                <tr>
                    <td>Порт выгрузки</td>
                    <td><?= Html::encode($item->getPod() ? $item->getPod()->getName() : '-') ?></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-1"></div>

        <div class="col-5">
            <h5 class="mb-3">Даты морской перевозки</h5>
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Планируемые</th>
                        <th>Фактические</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Дата букинга</td>
                        <td></td>
                        <td><?= $item->getCarrierBookingDate() ? $item->getCarrierBookingDate()->format('d.m.Y') : '' ?></td>
                    </tr>
                    <tr>
                        <td>Дата расчета ставки</td>
                        <td></td>
                        <td><?= $item->getPcdEffective() ? $item->getPcdEffective()->format('d.m.Y') : '' ?></td>
                    </tr>
                    <tr>
                        <td>Контейнер в порту отправления</td>
                        <td></td>
                        <td><?= $item->getGateInDate() ? $item->getGateInDate()->format('d.m.Y') : '' ?></td>
                    </tr>
                    <tr>
                        <td>Погрузка на борт</td>
                        <td></td>
                        <td><?= $item->getLoadOnBoardDate() ? $item->getLoadOnBoardDate()->format('d.m.Y') : '' ?></td>
                    </tr>
                    <tr>
                        <td>Отправление судна</td>
                        <td><?= $item->getEts() ? $item->getEts()->format('d.m.Y') : '' ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Приход в порт перегрузки</td>
                        <td><?= $item->getEtaTrans() ? $item->getEtaTrans()->format('d.m.Y') : '' ?></td>
                        <td><?= $item->getAtaTrans() ? $item->getAtaTrans()->format('d.m.Y') : '' ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php
        $invoices = [];

        foreach ($item->getInvoiceLines() as $invoiceLine) {
            /** @var \App\Entity\InvoiceLineEntity $invoiceLine */
            $invoice = $invoiceLine->getInvoice();
            if (!array_key_exists($invoice->getInvoiceId(), $invoices)) {
                $invoices[$invoice->getInvoiceId()] = [
                    'name' => $invoice->getName(),
                    'customer_account' => $invoice->getCustomerAccount() ? $invoiceLine->getInvoice()->getCustomerAccount()->getName() : '-',
                    'doc_date' => $invoice->getDocDate() ? $invoice->getDocDate()->format('d.m.Y') : '-',
                    'due_date' => $invoice->getDueDate() ? $invoice->getDueDate()->format('d.m.Y') : '-',
                    'amount' => [$invoice->getAmount(), $invoiceLine->getAmount()],
                    'currency' => $invoice->getCurrency() ?: '-',
                    'status' => $invoice->getStatus()
                ];

                continue;
            }

            $invoices[$invoice->getInvoiceId()]['amount'][1] = $invoices[$invoice->getInvoiceId()]['amount'][1] + $invoiceLine->getAmount();
        }
    ?>

    <div class="row justify-content-start mt-5">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Счёт №</th>
                        <th>Юр. лицо клиента</th>
                        <th>Дата документа</th>
                        <th>Срок оплаты</th>
                        <th>Сумма</th>
                        <th>Валюта</th>
                        <th>Статус</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($invoices as $id => $invoice): ?>
                        <tr>
                            <td>
                                <?= Html::a($invoice['name'], $urlGenerator->generate('invoices.detail', ['id' => $id])) ?>
                            </td>
                            <td>
                                <?= Html::encode($invoice['customer_account']) ?>
                            </td>
                            <td>
                                <?= Html::encode($invoice['doc_date']) ?>
                            </td>
                            <td>
                                <?= Html::encode($invoice['due_date']) ?>
                            </td>
                            <td>
                                <?= Html::encode($invoice['amount'][0]) ?><br>
                                <b><?= Html::encode($invoice['amount'][1]) ?></b>
                            </td>
                            <td>
                                <?= Html::encode($invoice['currency']) ?>
                            </td>
                            <td>
                                <?php if ($invoice['status'] == 22): ?>
                                    <span class="btn-sm btn-success">Оплачен</span>
                                <?php else: ?>
                                    Не оплачен
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
