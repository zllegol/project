<?php

/* @var \App\Shipment\Form\ShipmentsFilterForm $filterForm */
/** @var array $filterOptions */
/** @var array $gridData */
/* @var string $csrf */
/* @var \Yiisoft\Router\UrlGeneratorInterface $urlGenerator */

use Yiisoft\Form\Widget\Form;
use Yiisoft\Html\Html;

$formOptions = ['csrf' => $csrf];

foreach ($filterOptions as $key => $value) {
    if ($filterForm->hasAttribute($key)) {
        $filterForm->setAttribute($key, $value);
    }
}
/** @var \Yiisoft\Data\Paginator\OffsetPaginator $paginator */
$paginator = $gridData['paginator'];

?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col col-md-12 col-sm-12">
            <?= Form::widget()->action($urlGenerator->generate('shipments'))->options($formOptions)->begin() ?>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'cc', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\TextArea::widget()->config($filterForm, 'cc', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'ets_from', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'ets_from', ['class' => 'form-control']) ?>
                <span>&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'ets_to', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'eta_from', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'eta_from', ['class' => 'form-control']) ?>
                <span>&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                <?= \Yiisoft\Form\Widget\DatePicker::widget()->config($filterForm, 'eta_to', ['class' => 'form-control']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'pol_id', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DropDownList::widget()->config($filterForm, 'pol_id', ['class' => 'form-select'])->items(['' => '-'] + $filterOptions['port_select_options']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= \Yiisoft\Form\Widget\Label::widget()->config($filterForm, 'pod_id', ['class' => 'form-label']) ?>
                <?= \Yiisoft\Form\Widget\DropDownList::widget()->config($filterForm, 'pod_id', ['class' => 'form-select'])->items(['' => '-'] + $filterOptions['port_select_options']) ?>
            </div>

            <div class="mb-2 p-2">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Очистить', $urlGenerator->generate('shipments', ['filter_clear' => 1]), ['class' => 'btn btn-danger']) ?>
            </div>
            <?= Form::end() ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Контейнер №
                    </th>
                    <th>
                        Коносамент
                    </th>
                    <th>
                        Порт погрузки
                    </th>
                    <th>
                        Порт выгрузки
                    </th>
                    <th>
                        Предполагаемая дата отправления судна
                    </th>
                    <th>
                        Предполагаемая дата прибытия судна
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($paginator->read() as $item): ?>
                    <?php /** @var \App\Entity\ShipmentEntity $item */ ?>
                    <tr>
                        <td>
                            <?= Html::a(
                                $item->getContainerName() ? $item->getContainerName() : 'ДЕТАЛИ',
                                $urlGenerator->generate('shipments.detail', ['id' => $item->getShipmentId()])
                            ) ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getBlNumber()); ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getPol() ? $item->getPol()->getName() : '-') ?>
                        </td>
                        <td>
                            <?= Html::encode($item->getPod() ? $item->getPod()->getName() : '-') ?>
                        </td>
                        <td>
                            <?= $item->getEts() ? $item->getEts()->format('d.m.Y') : '-' ?>
                        </td>
                        <td>
                            <?= $item->getEta() ? $item->getEta()->format('d.m.Y') : '-' ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <?= \App\Widget\OffsetPagination::widget()->paginator($paginator)->urlGenerator(fn ($page) => $urlGenerator->generate('shipments', ['page' => $page])) ?>
    </div>
</div>

