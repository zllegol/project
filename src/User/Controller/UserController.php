<?php

namespace App\User\Controller;

use App\Repo\UserRepo;
use App\User\Form\LoginForm;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Yiisoft\Auth\IdentityRepositoryInterface;
use Yiisoft\Http\Method;
use Yiisoft\Router\UrlGeneratorInterface;
use Yiisoft\Session\SessionInterface;
use Yiisoft\User\CurrentUser;
use Yiisoft\Yii\View\ViewRenderer;

class UserController
{
    private ResponseFactoryInterface $responseFactory;
    private UrlGeneratorInterface $urlGenerator;
    private ViewRenderer $viewRenderer;
    private CurrentUser $currentUser;
    /** @var IdentityRepositoryInterface|UserRepo */
    private IdentityRepositoryInterface $identityRepository;

    /**
     * @param ResponseFactoryInterface $responseFactory
     * @param UrlGeneratorInterface $urlGenerator
     * @param ViewRenderer $viewRenderer
     * @param CurrentUser $currentUser
     */
    public function __construct(
        ResponseFactoryInterface $responseFactory,
        UrlGeneratorInterface $urlGenerator,
        ViewRenderer $viewRenderer,
        CurrentUser $currentUser,
        IdentityRepositoryInterface $identityRepository
    ) {
        $this->responseFactory = $responseFactory;
        $this->urlGenerator = $urlGenerator;
        $this->viewRenderer = $viewRenderer->withControllerName('user');
        $this->currentUser = $currentUser;
        $this->identityRepository = $identityRepository;
    }

    public function loginAction(ServerRequestInterface $request): ResponseInterface
    {
        if (!$this->currentUser->isGuest()) {
            return $this->responseFactory
                ->createResponse(302)
                ->withHeader(
                    'Location',
                    $this->urlGenerator->generate('home')
                );
        }

        $form = new LoginForm();

        $error = '';
        if ($request->getMethod() === Method::POST) {
            $body = $request->getParsedBody();
            $body = $body[$form->getFormName()] ?? [];

            try {
                foreach (['email' => 'Email', 'password' => 'Пароль'] as $name => $msg) {
                    if (empty($body[$name])) {
                        throw new \InvalidArgumentException(ucfirst($msg) . ' требуется для входа!');
                    }
                }

                $identity = $this->identityRepository->findOne(['email' => $body['email']]);

                if ($identity === null || !$identity->validatePassword($body['password'])) {
                    throw new \InvalidArgumentException('Неверный email или пароль');
                }

                if ($this->currentUser->login($identity)) {
                    return $this->responseFactory
                        ->createResponse(302)
                        ->withHeader(
                            'Location',
                            $this->urlGenerator->generate('home')
                        );
                }

                throw new \InvalidArgumentException('Невозможно войти');
            } catch (\Throwable $ex) {
                $error = $ex->getMessage();
            }
        }

        return $this->viewRenderer->render('login', ['form' => $form, 'error' => $error]);
    }

    public function logoutAction(ServerRequestInterface $request): ResponseInterface
    {
        if (!$this->currentUser->isGuest()) {
            $this->currentUser->logout();
        }

        return $this->responseFactory
            ->createResponse(302)
            ->withHeader(
                'Location',
                $this->urlGenerator->generate('login')
            );
    }
}
