<?php

namespace App\User\Form;

use Yiisoft\Form\FormModel;

class LoginForm extends FormModel
{
    private string $email = '';
    private string $password = '';

    public function getAttributeLabels(): array
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль'
        ];
    }
}
