<?php

namespace App\User\Middleware;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Yiisoft\Router\UrlGeneratorInterface;
use Yiisoft\Router\UrlMatcherInterface;
use Yiisoft\User\CurrentUser;

class UserMiddleware implements MiddlewareInterface
{
    private CurrentUser $currentUser;

    private UrlMatcherInterface $matcher;

    private ResponseFactoryInterface $responseFactory;

    private UrlGeneratorInterface $urlGenerator;

    private array $availableGuestRoutes = ['login'];

    /**
     * @param CurrentUser $currentUser
     */
    public function __construct(CurrentUser $currentUser, UrlMatcherInterface $matcher, ResponseFactoryInterface $responseFactory, UrlGeneratorInterface $urlGenerator)
    {
        $this->currentUser = $currentUser;
        $this->matcher = $matcher;
        $this->responseFactory = $responseFactory;
        $this->urlGenerator = $urlGenerator;
    }


    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $result = $this->matcher->match($request);

        if (!$result->isSuccess()) {
            return $handler->handle($request);
        }

        $isGuest = $this->currentUser->isGuest();

        if (!$isGuest || ($isGuest && in_array($result->route()->getName(), $this->availableGuestRoutes))) {
            return $handler->handle($request);
        }

        return $this->responseFactory->createResponse(302)->withHeader('Location', $this->urlGenerator->generate('login'));
    }
}
