<?php

namespace App\User\Command;

use App\Entity\InvoiceEntity;
use App\Entity\UserEntity;
use App\Repo\UserRepo;
use Cycle\ORM\ORM;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Yiisoft\Yii\Console\ExitCode;
use Yiisoft\Yii\Cycle\Data\Writer\EntityWriter;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'user/create';

    private ORM $orm;

    /**
     * @param ORM $orm
     */
    public function __construct(ORM $orm)
    {
        parent::__construct();
        $this->orm = $orm;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a user')
            ->setHelp('This command allows you to create a user')
            ->addArgument('email', InputArgument::REQUIRED, 'Email')
            ->addArgument('password', InputArgument::REQUIRED, 'Password');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $email = $input->getArgument('email');
        $password = $input->getArgument('password');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $io->error('Invalid email');
            return ExitCode::UNSPECIFIED_ERROR;
        }

        if (mb_strlen($password) < 5) {
            $io->error('Password too short');
            return ExitCode::UNSPECIFIED_ERROR;
        }

        /** @var UserRepo $userRepo */
        $userRepo = $this->orm->getRepository(UserEntity::class);

        if ($userRepo->findOne(['email' => $email])) {
            $io->error('User with same email already exists');
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $user = new UserEntity();
        $user->setEmail($email);
        $user->setName($email);
        $user->setPassword($password);
        $user->setCreatedAt(date('Y-m-d H:i:s'));

        try {
            (new EntityWriter($this->orm))->write([$user]);
            $io->success('User created');
        } catch (\Throwable $ex) {
            $io->error($ex->getMessage());
            return ExitCode::UNSPECIFIED_ERROR;
        }

        return ExitCode::OK;
    }
}
