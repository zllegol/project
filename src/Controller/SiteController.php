<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\InvoiceEntity;
use App\Entity\ShipmentEntity;
use App\Repo\InvoiceRepo;
use App\Repo\ShipmentRepo;
use Cycle\ORM\ORM;
use Psr\Http\Message\ResponseInterface;
use Yiisoft\Yii\View\ViewRenderer;

class SiteController
{
    private ViewRenderer $viewRenderer;

    private ORM $orm;

    public function __construct(ViewRenderer $viewRenderer, ORM $orm)
    {
        $this->viewRenderer = $viewRenderer->withControllerName('site');
        $this->orm = $orm;
    }

    public function index(): ResponseInterface
    {
        /** @var ShipmentRepo $shipmentRepo */
        $shipmentRepo = $this->orm->getRepository(ShipmentEntity::class);

        /** @var InvoiceRepo $invoiceRepo */
        $invoiceRepo = $this->orm->getRepository(InvoiceEntity::class);

        $data = [
            'shipment_count' => $shipmentRepo->select()->count(),
            'invoice_count' => $invoiceRepo->select()->count(),
        ];
        return $this->viewRenderer->render('index', $data);

    }
}
