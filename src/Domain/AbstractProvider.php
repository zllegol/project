<?php

namespace App\Domain;

use Cycle\ORM\ORM;
use Doctrine\Common\Collections\Collection;
use Yiisoft\Data\Paginator\OffsetPaginator;
use Yiisoft\Session\SessionInterface;
use Yiisoft\Yii\Cycle\Data\Reader\EntityReader;

abstract class AbstractProvider extends AppService
{
    protected const DEFAULT_PAGE_SIZE = 20;

    protected SessionInterface $session;

    protected ORM $orm;

    public function __construct(SessionInterface $session, ORM $orm)
    {
        $this->session = $session;
        $this->orm = $orm;
    }

    protected function job(Collection $options): mixed
    {
        if ($options->get('filter_clear')) {
            return $this->clearFilter();
        }

        if ($options->get('filter')) {
            return $this->filter($options);
        }

        if ($options->get('filter_options')) {
            return $this->filterOptions();
        }

        if ($options->get('grid')) {
            return $this->grid($options);
        }

        if ($options->get('detail')) {
            return $this->detail($options);
        }

        throw new \UnexpectedValueException('Undefined job');
    }

    abstract protected function filterOptions(bool $withInitOptions = true): array;

    abstract protected function grid(Collection $options): array;

    abstract protected function detail(Collection $options): mixed;

    abstract protected function getSessionNamespace(): string;

    protected function filter(Collection $options): bool
    {
        $filterData = $options->get('filter_data') ?? [];

        $this->session->set($this->getSessionNamespace(), $filterData);

        return true;
    }

    protected function clearFilter(): bool
    {
        $this->session->set($this->getSessionNamespace(), []);
        return true;
    }

    protected function createPaginator(EntityReader $reader, int $currentPage): OffsetPaginator
    {
        $paginator = new OffsetPaginator($reader);
        return $paginator->withPageSize(self::DEFAULT_PAGE_SIZE)->withCurrentPage($currentPage);
    }

    protected function getCurrentPageNumber(Collection $options): int
    {
        $page = $options->get('request_data')['page'] ?? 1;
        return $page < 1 ? 1 : $page;
    }
}
