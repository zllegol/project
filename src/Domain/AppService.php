<?php

namespace App\Domain;

use App\Domain\Options\Result;
use App\Domain\Options\ResultInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

abstract class AppService
{
    public function serve(array $options): ResultInterface
    {
        $result = new Result();

        try {
            $requestOptions = new ArrayCollection($options);
            $result->setResult($this->job($requestOptions));
        } catch (\Throwable $ex) {
            $result->setError($ex);
        }

        return $result;
    }

    abstract protected function job(Collection $options): mixed;
}
