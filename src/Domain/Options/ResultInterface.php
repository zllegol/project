<?php

namespace App\Domain\Options;

interface ResultInterface
{
    public function getResult();


    public function setResult(mixed $result);


    public function getError(): ?\Throwable;


    public function setError(mixed $error);


    public function isError(): bool;

    public function thrErrIfExist();
}
