<?php

namespace App\Domain\Options;

use Doctrine\Common\Collections\ArrayCollection;

class Result extends ArrayCollection implements ResultInterface
{
    public function getResult()
    {
        return $this->get('result');
    }

    public function setResult(mixed $result)
    {
        $this->set('result', $result);
    }

    public function getError(): ?\Throwable
    {
        return $this->get('error');
    }

    public function setError(mixed $error)
    {
        $this->set('error', $error);
    }

    public function isError(): bool
    {
        return null !== $this->get('error');
    }

    public function thrErrIfExist()
    {
        if ($this->isError() && $this->getError()) {
            throw $this->getError();
        }
    }
}
