<?php

namespace App\Shipment\Form;

use Yiisoft\Form\FormModel;

class ShipmentsFilterForm extends FormModel
{
    private string $cc = '';
    private string $ets_from = '';
    private string $ets_to = '';
    private string $eta_from = '';
    private string $eta_to = '';
    private string $pol_id = '';
    private string $pod_id = '';

    /**
     * @return string
     */
    public function getCc(): string
    {
        return $this->cc;
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getEtsFrom(): ?\DateTime
    {
        return $this->ets_from ? new \DateTime($this->eta_from) : null;
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getEtsTo(): ?\DateTime
    {
        return $this->ets_to ? new \DateTime($this->ets_to) : null;
    }

    /**
     * @return string
     */
    public function getEtaFrom(): string
    {
        return $this->eta_from;
    }

    /**
     * @return string
     */
    public function getEtaTo(): string
    {
        return $this->eta_to;
    }

    /**
     * @return string
     */
    public function getPolId(): string
    {
        return $this->pol_id;
    }

    /**
     * @return string
     */
    public function getPodId(): string
    {
        return $this->pod_id;
    }

    public function getAttributeLabels(): array
    {
        return [
            'cc' => 'Контейнер или Коносамент',
            'ets_from' => 'Предпологаемаея дата отправления судна',
            'ets_to' => '',
            'eta_from' => 'Предпологаемаея дата прибытия судна',
            'eta_to' => '',
            'pol_id' => 'Порт погрузки',
            'pod_id' => 'Порт выгрузки'
        ];
    }
}
