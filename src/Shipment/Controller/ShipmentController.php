<?php

namespace App\Shipment\Controller;

use App\Shipment\Form\ShipmentsFilterForm;
use App\Shipment\Service\ShipmentProvider;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Yiisoft\Router\UrlGeneratorInterface;
use Yiisoft\Yii\View\ViewRenderer;

class ShipmentController
{
    private ResponseFactoryInterface $responseFactory;
    private ViewRenderer $viewRenderer;
    private UrlGeneratorInterface $urlGenerator;
    private ShipmentProvider $shipmentGridProvider;

    /**
     * @param ResponseFactoryInterface $responseFactory
     * @param ViewRenderer $viewRenderer
     * @param UrlGeneratorInterface $urlGenerator
     * @param ShipmentProvider $shipmentGridProvider
     */
    public function __construct(ResponseFactoryInterface $responseFactory, ViewRenderer $viewRenderer, UrlGeneratorInterface $urlGenerator, ShipmentProvider $shipmentGridProvider)
    {
        $this->responseFactory = $responseFactory;
        $this->viewRenderer = $viewRenderer->withControllerName('shipment');
        $this->urlGenerator = $urlGenerator;
        $this->shipmentGridProvider = $shipmentGridProvider;
    }

    public function indexAction(ServerRequestInterface $request): ResponseInterface
    {
        $filterForm = new ShipmentsFilterForm();

        if ($request->getMethod() == 'POST') {
            $params = $request->getParsedBody();

            if (isset($params[$filterForm->getFormName()])) {
                $this->shipmentGridProvider->serve(['filter' => true, 'filter_data' => $params[$filterForm->getFormName()]]);
            }

            return $this->responseFactory
                ->createResponse(302)
                ->withHeader('Location', $this->urlGenerator->generate('shipments'));
        }

        if (isset($request->getQueryParams()['filter_clear'])) {
            $this->shipmentGridProvider->serve(['filter_clear' => true]);
            return $this->responseFactory
                ->createResponse(302)
                ->withHeader('Location', $this->urlGenerator->generate('shipments'));
        }

        $result = $this->shipmentGridProvider->serve(['filter_options' => true]);
        $result->thrErrIfExist();
        $filterOptions = $result->getResult();

        $result = $this->shipmentGridProvider->serve(['grid' => true, 'request_data' => $request->getQueryParams()]);
        $result->thrErrIfExist();
        $gridData = $result->getResult();

        return $this->viewRenderer->render('index', [
            'filterForm' => $filterForm,
            'gridData' => $gridData,
            'filterOptions' => $filterOptions
        ]);
    }

    public function detailAction(ServerRequestInterface $request): ResponseInterface
    {
        $res = $this->shipmentGridProvider->serve(['detail' => true] + $request->getAttributes());

        $res->thrErrIfExist();

        return $this->viewRenderer->render('detail', ['item' => $res->getResult()]);
    }
}
