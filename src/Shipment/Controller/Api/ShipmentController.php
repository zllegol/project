<?php

namespace App\Shipment\Controller\Api;

use App\Shipment\Service\ShipmentSaver;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ShipmentController
{
    private ResponseFactoryInterface $responseFactory;

    private ShipmentSaver $shipmentSaver;

    /**
     * @param ResponseFactoryInterface $responseFactory
     * @param ShipmentSaver $shipmentSaver
     */
    public function __construct(ResponseFactoryInterface $responseFactory, ShipmentSaver $shipmentSaver)
    {
        $this->responseFactory = $responseFactory;
        $this->shipmentSaver = $shipmentSaver;
    }

    public function postAction(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $body = json_decode(trim($request->getBody()->getContents()), true);
            if (!$body) {
                throw new \Error();
            }
        } catch (\Throwable $ex) {
            return $this->responseFactory->createResponse(400);
        }

        $result = $this->shipmentSaver->serve($body);

        $error = '';
        try {
            $result->thrErrIfExist();
        } catch (\InvalidArgumentException $ex) {
            $error = $ex->getMessage();
        } catch (\Throwable $ex) {
            $error = 'Internal error ' . $ex->getMessage();
        }

        $data = ['status' => true];

        if ($error) {
            $data['status'] = false;
            $data['msg'] = $error;
        } else {
            $data['data'] = $result->getResult();
        }

        $response = $this->responseFactory->createResponse($error ? 400 : 201);
        $response = $response->withHeader('Content-Type', 'application/json');
        $response->getBody()->write(json_encode($data));
        return $response;
    }
}
