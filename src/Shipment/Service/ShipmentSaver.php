<?php

namespace App\Shipment\Service;

use App\Domain\AppService;
use App\Entity\ContainerTypeEntity;
use App\Entity\PortEntity;
use App\Entity\ShipmentEntity;
use App\Entity\UserEntity;
use App\Entity\VendorEntity;
use App\Repo\ContainerTypeRepo;
use App\Repo\PortRepo;
use App\Repo\ShipmentRepo;
use App\Repo\UserRepo;
use Cycle\ORM\ORM;
use Cycle\ORM\Transaction;
use Doctrine\Common\Collections\Collection;
use Yiisoft\Validator\Result;
use Yiisoft\Validator\Rule\HasLength;
use Yiisoft\Validator\Rule\Number;
use Yiisoft\Validator\Rule\Required;
use Yiisoft\Validator\Rules;

class ShipmentSaver extends AppService
{
    private ORM $orm;

    /**
     * @param ORM $orm
     */
    public function __construct(ORM $orm)
    {
        $this->orm = $orm;
    }

    protected function job(Collection $options): mixed
    {
        $entity = $this->getEntity($options);

        $this->validateEntity($entity, $options);

        $entity->setContainerName($options->get('container_name'));
        $entity->setBlNumber($options->get('bl_number'));
        $entity->setSealNumber($options->get('seal_number'));
        $entity->setAtaTrans($options->get('ata_trans'));
        $entity->setCarrierBookingDate($options->get('carrier_booking_date'));
        $entity->setContainerTypeId($options->get('container_type_id'));
        $entity->setEta($options->get('eta'));
        $entity->setEtaTrans($options->get('eta_trans'));
        $entity->setEts($options->get('ets'));
        $entity->setPcdEffective($options->get('pcdeffective'));
        $entity->setGateInDate($options->get('gate_in_date'));
        $entity->setLoadOnBoardDate($options->get('load_on_board_date'));
        $entity->setPodId($options->get('pod_id'));
        $entity->setResponsibleCsId($options->get('responsible_cs_id'));
        $entity->setShippingLineId($options->get('shipping_line_id'));

        $tr = new Transaction($this->orm);
        $tr->persist($entity);
        $tr->run();

        $res = [];

        $res['shipment_id'] = $entity->getShipmentId();
        $res['container_name'] = $entity->getContainerName();
        $res['bl_number'] = $entity->getBlNumber();
        $res['seal_number'] = $entity->getSealNumber();
        $res['carrier_booking_date'] = $entity->getAtaTrans();
        $res['container_type_id'] = $entity->getContainerTypeId();
        $res['eta'] = $entity->getEta();
        $res['eta_trans'] = $entity->getEtaTrans();
        $res['ets'] = $entity->getEts();
        $res['pcdeffective'] = $entity->getPcdEffective();
        $res['gate_in_date'] = $entity->getGateInDate();
        $res['load_on_board_date'] = $entity->getLoadOnBoardDate();
        $res['pod_id'] = $entity->getPodId();
        $res['responsible_cs_id'] = $entity->getResponsibleCsId();
        $res['shipping_line_id'] = $entity->getShippingLineId();

        return $res;
    }


    private function validateEntity(ShipmentEntity $entity, Collection $options)
    {
        $containerRules = new Rules([
            Required::rule(),
            HasLength::rule()->min(5)->max(50)
        ]);

        $this->throwErrorMessageIfNeeded($containerRules->validate($options->get('container_name')), 'container_name');
        $this->throwErrorMessageIfNeeded($containerRules->validate($options->get('seal_number')), 'seal_number');

        $blNumberRules = new Rules([
            Required::rule(),
            HasLength::rule()->min(3)->max(1000)
        ]);

        $this->throwErrorMessageIfNeeded($blNumberRules->validate($options->get('bl_number')), 'bl_number');

        if ($options->get('ata_trans')) {
            if (!date_create_from_format('Y-m-d', $options->get('ata_trans'))) {
                throw new \InvalidArgumentException('Invalid ata_trans');
            }
        }

        if ($options->get('carrier_booking_date')) {
            if (!date_create_from_format('Y-m-d', $options->get('carrier_booking_date'))) {
                throw new \InvalidArgumentException('Invalid carrier_booking_date');
            }
        }

        /** @var ContainerTypeRepo $containerTypeRepo */
        $containerTypeRepo = $this->orm->getRepository(ContainerTypeEntity::class);
        if (!$containerTypeRepo->findByPK((int) $options->get('container_type_id'))) {
            throw new \InvalidArgumentException('Invalid container_type_id');
        }

        if ($options->get('eta')) {
            if (!date_create_from_format('Y-m-d', $options->get('eta'))) {
                throw new \InvalidArgumentException('Invalid eta');
            }
        }

        if ($options->get('eta_trans')) {
            if (!date_create_from_format('Y-m-d', $options->get('eta_trans'))) {
                throw new \InvalidArgumentException('Invalid eta_trans');
            }
        }

        if ($options->get('ets')) {
            if (!date_create_from_format('Y-m-d', $options->get('ets'))) {
                throw new \InvalidArgumentException('Invalid ets');
            }
        }
        if ($options->get('pcdeffective')) {
            if (!date_create_from_format('Y-m-d', $options->get('pcdeffective'))) {
                throw new \InvalidArgumentException('Invalid pcdeffective');
            }
        }

        if ($options->get('gate_in_date')) {
            if (!date_create_from_format('Y-m-d', $options->get('gate_in_date'))) {
                throw new \InvalidArgumentException('Invalid gate_in_date');
            }
        }

        if ($options->get('load_on_board_date')) {
            if (!date_create_from_format('Y-m-d', $options->get('load_on_board_date'))) {
                throw new \InvalidArgumentException('Invalid load_on_board_date');
            }
        }

        /** @var PortRepo $portRepo */
        $portRepo = $this->orm->getRepository(PortEntity::class);
        if (!$portRepo->findByPK((int) $options->get('pod_id'))) {
            throw new \InvalidArgumentException('Invalid pod_id');
        }

        /** @var UserRepo $userRepo */
        $userRepo = $this->orm->getRepository(UserEntity::class);
        if (!$userRepo->findByPK((int) $options->get('responsible_cs_id'))) {
            throw new \InvalidArgumentException('Invalid responsible_cs_id');
        }

        $vendorRepo = $this->orm->getRepository(VendorEntity::class);
        if (!$vendorRepo->findByPK((int) $options->get('shipping_line_id'))) {
            throw new \InvalidArgumentException('Invalid shipping_line_id');
        }
    }

    /**
     * @param Collection $options
     * @return ShipmentEntity|object
     */
    private function getEntity(Collection $options): ShipmentEntity
    {
        $entity = null;
        if ($options->get('shipment_id')) {
            $entity = $this->orm->getRepository(ShipmentEntity::class)->findByPK($options->get('shipment_id'));
        }

        if (!$entity) {
            $entity = new ShipmentEntity();
        }

        return $entity;
    }

    private function throwErrorMessageIfNeeded(Result $result, $key)
    {
        if (!$result->isValid()) {
            throw new \InvalidArgumentException($key . ': ' . implode(PHP_EOL, $result->getErrors()));
        }
    }
}
