<?php

namespace App\Shipment\Service;

use App\Domain\AbstractProvider;
use App\Entity\InvoiceEntity;
use App\Entity\InvoiceLineEntity;
use App\Entity\PortEntity;
use App\Entity\ShipmentEntity;
use App\Repo\InvoiceLineRepo;
use App\Repo\InvoiceRepo;
use App\Repo\PortRepo;
use App\Repo\ShipmentRepo;
use Cycle\ORM\Select\QueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Spiral\Database\Injection\Parameter;
use Yiisoft\Data\Paginator\OffsetPaginator;
use Yiisoft\Yii\Cycle\Data\Reader\EntityReader;

class ShipmentProvider extends AbstractProvider
{
    protected function filterOptions(bool $withInitOptions = true): array
    {
        /** @var PortRepo $portRepo */
        $portRepo = $this->orm->getRepository(PortEntity::class);

        $filterData = $this->session->get($this->getSessionNamespace(), []);

        $res = [
            'cc' => $filterData['cc'] ?? '',
            'ets_from' => $filterData['ets_from'] ?? '',
            'ets_to' => $filterData['ets_to'] ?? '',
            'eta_from' => $filterData['eta_from'] ?? '',
            'eta_to' => $filterData['eta_to'] ?? '',
            'pol_id' => $filterData['pol_id'] ?? '',
            'pod_id' => $filterData['pod_id'] ?? ''
        ];

        if ($withInitOptions) {
            $res['port_select_options'] = $portRepo->getSelectOptions();
        }

        return $res;
    }

    protected function grid(Collection $options): array
    {
        $filterOptions = $this->filterOptions(false);

        $filterOptions = array_filter($filterOptions);

        /** @var ShipmentRepo $shipmentRepo */
        $shipmentRepo = $this->orm->getRepository(ShipmentEntity::class);

        $select = $shipmentRepo->select();

        foreach ($filterOptions as $key => $value) {
            switch ($key) {
                case 'cc':
                    $select->andwhere(function (QueryBuilder $select) use ($value) {
                        $value = '%' . $value . '%';
                        $select->where('container_name', 'LIKE', $value)->orWhere('bl_number', 'LIKE', $value);
                    });
                    break;
                case 'pod_id':
                    $select->andWhere('pod_id', '=', $value);
                    break;
                case 'pol_id':
                    $select->andWhere('pol_id', '=', $value);
                    break;
            }
        }

        if (isset($filterOptions['ets_from']) && isset($filterOptions['ets_to'])) {
            $select->andWhere(function (QueryBuilder $select) use ($filterOptions) {
                $select->where('ets', '>=', $filterOptions['ets_from'])->andWhere('ets', '<=', $filterOptions['ets_to']);
            });
        } elseif (isset($filterOptions['ets_from'])) {
            $select->andWhere('ets', '>=', $filterOptions['ets_from']);
        } elseif (isset($filterOptions['ets_to'])) {
            $select->andWhere('ets', '<=', $filterOptions['ets_to']);
        }

        if (isset($filterOptions['eta_from']) && isset($filterOptions['eta_to'])) {
            $select->andWhere(function (QueryBuilder $select) use ($filterOptions) {
                $select->where('eta', '>=', $filterOptions['eta_from'])->andWhere('eta', '<=', $filterOptions['eta_to']);
            });
        } elseif (isset($filterOptions['eta_from'])) {
            $select->andWhere('eta', '>=', $filterOptions['eta_from']);
        } elseif (isset($filterOptions['eta_to'])) {
            $select->andWhere('eta', '<=', $filterOptions['eta_to']);
        }

        $select = $select->load(['pol', 'pod', 'containerType']);

        $reader = new EntityReader($select);

        $page = $this->getCurrentPageNumber($options);

        $paginator = $this->createPaginator($reader, $page);

        return ['paginator' => $paginator, 'page' => $page, 'page_count' => $paginator->getTotalPages()];
    }

    protected function detail(Collection $options): mixed
    {
        $id = $options->get('id');
        if (!$id) {
            throw new \UnexpectedValueException('Id is missing');
        }

        /** @var ShipmentRepo $shipmentRepo */
        $shipmentRepo = $this->orm->getRepository(ShipmentEntity::class);

        /** @var ShipmentEntity $item */
        $item = $shipmentRepo->select()->wherePK($id)->load(['pol', 'pod', 'invoiceLines', 'containerType'])->fetchOne();

        if (!$item) {
            throw new \InvalidArgumentException('Invalid id passed');
        }

        /** @var InvoiceLineRepo $invoiceLineRepo */
        $invoiceLineRepo = $this->orm->getRepository(InvoiceLineEntity::class);

        $invoiceLines = $invoiceLineRepo->select()->where('shipment_id', '=', $item->getShipmentId())->load('invoice.customerAccount')->fetchAll();

        $item->setInvoiceLines(new ArrayCollection($invoiceLines));

        return $item;

        // old
        $invoiceIds = array_unique(array_filter(array_map(function (InvoiceLineEntity $entity) {
            return $entity->getInvoiceId();
        }, $item->getInvoiceLines()->getValues())));

        if (!$invoiceIds) {
            return $item;
        }

        /** @var InvoiceRepo $invoiceRepo */
        $invoiceRepo = $this->orm->getRepository(InvoiceEntity::class);

        $invoices = $invoiceRepo->select()->where('id', 'in', new Parameter($invoiceIds))->load(['customerAccount'])->fetchAll();

        $tmp = [];
        foreach ($invoices as $invoice) {
            $tmp[$invoice->getInvoiceId()] = $invoice;
        }
        $invoices = $tmp;

        $collection = new ArrayCollection();
        foreach ($item->getInvoiceLines()->getIterator() as $value) {
            if (array_key_exists($value->getInvoiceId(), $invoices)) {
                $value->setInvoice($invoices[$value->getInvoiceId()]);
            }
            $collection->add($value);
        }

        $item->setInvoiceLines($collection);

        return $item;
    }

    protected function getSessionNamespace(): string
    {
        return __NAMESPACE__ . __CLASS__;
    }
}
