<?php

namespace App\Repo;

use Cycle\ORM\Select\Repository;

class PortRepo extends Repository
{
    public function getSelectOptions(): array
    {
        $res = [];

        $assoc = $this->select()
            ->buildQuery()
            ->columns(['id', 'name'])
            ->orderBy(['name' => 'ASC'])
            ->fetchAll();

        foreach ($assoc as $item) {
            $res[$item['id']] = $item['name'];
        }

        return $res;
    }
}
