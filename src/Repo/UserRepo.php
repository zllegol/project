<?php

namespace App\Repo;

use Cycle\ORM\Select\Repository;
use Yiisoft\Auth\IdentityInterface;
use Yiisoft\Auth\IdentityRepositoryInterface;

class UserRepo extends Repository implements IdentityRepositoryInterface
{
    /**
     * @param string $id
     * @return IdentityInterface|null|object
     */
    public function findIdentity(string $id): ?IdentityInterface
    {
        return $this->findByPK($id);
    }

}
