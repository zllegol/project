<?php

declare(strict_types=1);

namespace App\ViewInjection;

use App\ApplicationParameters;
use Yiisoft\Router\UrlGeneratorInterface;
use Yiisoft\Yii\View\ContentParametersInjectionInterface;

final class ContentViewInjection implements ContentParametersInjectionInterface
{
    private ApplicationParameters $applicationParameters;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        ApplicationParameters $applicationParameters,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->applicationParameters = $applicationParameters;
        $this->urlGenerator = $urlGenerator;
    }

    public function getContentParameters(): array
    {
        return [
            'applicationParameters' => $this->applicationParameters,
            'urlGenerator' => $this->urlGenerator
        ];
    }
}
