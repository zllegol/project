<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Relation\HasOne;
use Cycle\Annotated\Annotation\Table;
use Cycle\ORM\Promise\Reference;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @Entity(repository="App\Repo\InvoiceRepo", table="invoices")
 */
class InvoiceEntity
{
    /**
     * @var int|null
     *
     * @Column(type="primary", name="id", typecast="int")
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @Column(type = "string", name = "name")
     */
    private $name;

    /**
     * @var Reference|null|CustomerAccountEntity
     *
     * @HasOne(target = "App\Entity\CustomerAccountEntity", innerKey="customerAccountId", outerKey="customerAccountId")
     */
    private $customerAccount;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="customer_account_id")
     */
    private $customerAccountId;

    /**
     * @var Reference|null|CustomerAccountEntity
     *
     * @HasOne(target = "App\Entity\CustomerEntity", innerKey="customerId", outerKey="customerId")
     */
    private $customer;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="customer_id")
     */
    private $customerId;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="doc_date")
     */
    private $docDate;

    /**
     * @var  \DateTimeInterface
     *
     * @Column(type="date", name="due_date")
     */
    private $dueDate;


    /**
     * @var double
     *
     * @Column (type="decimal(11,2)", name="amount")
     */
    private $amount;

    /**
     * @var double
     *
     * @Column (type="decimal(11,2)", name="outstanding_amount")
     */
    private $outstandingAmount;

    /**
     * @var string
     *
     * @Column(type="string(3)", name="currency")
     */
    private $currency;

    /**
     * @var int
     *
     * @Column(type="int", name="status")
     */
    private $status;

    /**
     * @var ArrayCollection
     *
     * @HasMany (target = "App\Entity\InvoiceLineEntity", innerKey="invoiceId", outerKey="invoiceId")
     */
    private $invoiceLines;

    public function __construct()
    {
        $this->invoiceLines = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getInvoiceId(): ?int
    {
        return $this->invoiceId;
    }

    /**
     * @param int|null $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Reference|null|CustomerAccountEntity
     */
    public function getCustomerAccount()
    {
        return $this->customerAccount;
    }

    /**
     * @param Reference|null|CustomerAccountEntity $customerAccount
     */
    public function setCustomerAccount($customerAccount)
    {
        $this->customerAccount = $customerAccount;
    }

    /**
     * @return int
     */
    public function getCustomerAccountId()
    {
        return $this->customerAccountId;
    }

    /**
     * @param int $customerAccountId
     */
    public function setCustomerAccountId($customerAccountId)
    {
        $this->customerAccountId = $customerAccountId;
    }

    /**
     * @return  \DateTimeInterface
     */
    public function getDocDate()
    {
        return $this->docDate;
    }

    /**
     * @param  \DateTimeInterface $docDate
     */
    public function setDocDate($docDate)
    {
        $this->docDate = $docDate;
    }

    /**
     * @return  \DateTimeInterface
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param  \DateTimeInterface $dueDate
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus( $status)
    {
        $this->status = $status;
    }

    /**
     * @return ArrayCollection
     */
    public function getInvoiceLines(): ArrayCollection
    {
        return $this->invoiceLines;
    }

    /**
     * @param ArrayCollection $invoiceLines
     */
    public function setInvoiceLines($invoiceLines)
    {
        $this->invoiceLines = $invoiceLines;
    }

    /**
     * @return CustomerAccountEntity|Reference|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param CustomerAccountEntity|Reference|null $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int|null $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return float
     */
    public function getOutstandingAmount()
    {
        return $this->outstandingAmount;
    }

    /**
     * @param float $outstandingAmount
     */
    public function setOutstandingAmount($outstandingAmount)
    {
        $this->outstandingAmount = $outstandingAmount;
    }


}
