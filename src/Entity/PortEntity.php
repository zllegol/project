<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Table;

/**
 * @Entity(repository="App\Repo\PortRepo", table="ports")
 */
class PortEntity
{
    /**
     * @var int|null
     *
     * @Column(type="bigPrimary", name="id")
     */
    private $portId;

    /**
     * @var string
     *
     * @Column(type = "string", name = "name")
     */
    private $name;

    /**
     * @return int|null
     */
    public function getPortId()
    {
        return $this->portId;
    }

    /**
     * @param int|null $portId
     */
    public function setPortId($portId)
    {
        $this->portId = $portId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
