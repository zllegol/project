<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Table;

/**
 * @Entity(repository="App\Repo\ContainerTypeRepo", table="container_types")
 */
class ContainerTypeEntity
{
    /**
     * @var int|null
     *
     * @Column(type="bigPrimary", name="id")
     */
    private $containerTypeId;

    /**
     * @var string
     *
     * @Column(type = "string", name = "name")
     */
    private $name;

    /**
     * @return int|null
     */
    public function getContainerTypeId()
    {
        return $this->containerTypeId;
    }

    /**
     * @param int|null $containerTypeId
     */
    public function setContainerTypeId($containerTypeId)
    {
        $this->containerTypeId = $containerTypeId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
