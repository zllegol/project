<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Table;

/**
 * @Entity(repository="App\Repo\CustomerRepo", table="customers")
 */
class CustomerEntity
{
    /**
     * @var int|null
     *
     * @Column(type="primary", name="id")
     */
    private $customerId;

    /**
     * @var string
     *
     * @Column(type = "string", name = "name")
     */
    private $name;

    /**
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int|null $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
