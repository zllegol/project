<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Relation\HasOne;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\ORM\Promise\Reference;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity(repository="App\Repo\ShipmentRepo", table="shipments")
 */
class ShipmentEntity
{
    /**
     * @var int|null
     *
     * @Column(type="bigPrimary", name="id", typecast="int")
     */
    private $shipmentId;

    /**
     * @var string
     *
     * @Column (type="string(50)", name="container_name")
     */
    private $containerName;

    /**
     * @var string
     *
     * @Column (type="string(1000)", name="bl_number")
     */
    private $blNumber;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="ets")
     */
    private $ets;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="eta")
     */
    private $eta;

    /**
     * @var Reference|null|PortEntity
     *
     * @HasOne(target = "App\Entity\PortEntity", innerKey="polId", outerKey="portId")
     */
    private $pol;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="pol_id")
     */
    private $polId;

    /**
     * @var Reference|null|PortEntity
     *
     * @HasOne(target = "App\Entity\PortEntity", innerKey="podId", outerKey="portId")
     */
    private $pod;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="pod_id")
     */
    private $podId;

    /**
     * @var Reference|null|ContainerTypeEntity
     *
     * @HasOne(target = "App\Entity\ContainerTypeEntity", innerKey="containerTypeId", outerKey="containerTypeId")
     */
    private $containerType;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="container_type_id")
     */
    private $containerTypeId;

    /**
     * @var string
     *
     * @Column (type="string(50)", name="seal_number")
     */
    private $sealNumber;

    /**
     * @var Reference|null|UserEntity
     *
     * @HasOne(target = "App\Entity\UserEntity", innerKey="responsibleCsId", outerKey="userId")
     */
    private $responsibleCs;

    /**
     *
     * TODO: relation to User
     * @var int|null
     *
     * @Column(type="bigInteger", name="responsible_cs_id")
     */
    private $responsibleCsId;

    /**
     * @var Reference|null|VendorEntity
     *
     * @HasOne(target = "App\Entity\VendorEntity", innerKey="shippingLineId", outerKey="vendorId")
     */
    private $shippingLine;

    /**
     * @var int|null
     *
     * @Column(type="integer", name="file_closed")
     */
    private $fileClosed = 0;

    /**
     * @var int|null
     *
     * @Column(type="integer", name="trucking_status")
     */
    private $truckingStatus = 0;

    /**
     * @var int|null
     *
     * @Column(type="integer", name="container_weight")
     */
    private $containerWeight = 0;

    /**
     * @var int|null
     *
     * @Column(type="integer", name="container_volume")
     */
    private $containerVolume = 0;

    /**
     * @var int|null
     *
     * @Column(type="integer", name="dropoff")
     */
    private $dropoff = 0;


    /**
     * @var int|null
     *
     * @Column(type="integer", name="feeder_bl_received")
     */
    private $feederPlReceived = 0;

    /**
     * @var int|null
     *
     * @Column(type="integer", name="rail_status")
     */
    private $railStatus = 0;


    /**
     * @var int|null
     *
     * @Column(type="date", name="rail_pickup_from_destination_platform_date")
     */
    private $railPickupFromDestinationPlatformDate = '0000-00-00';

    /**
     * @var int|null
     *
     * @Column(type="decimal(9,2)", name="selling_rate_ocean")
     */
    private $sellingRateOcean = 0;

    /**
     * @var int|null
     *
     * @Column(type="decimal(9,2)", name="selling_rate_truck")
     */
    private $sellingRateTruck = 0;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="shipping_line_id")
     */
    private $shippingLineId;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="carrier_booking_date")
     */
    private $carrierBookingDate;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="pcdeffective")
     */
    private $pcdEffective;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="gate_in_date")
     */
    private $gateInDate;

    /**
     * @var \DateTime
     *
     * @Column(type="date", name="load_on_board_date")
     */
    private $loadOnBoardDate;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="eta_trans")
     */
    private $etaTrans;

    /**
     * @var \DateTimeInterface
     *
     * @Column(type="date", name="ata_trans")
     */
    private $ataTrans;

    /**
     * @var ArrayCollection
     *
     * @HasMany (target = "App\Entity\InvoiceLineEntity", innerKey="shipmentId", outerKey="shipmentId")
     */
    private $invoiceLines;

    /**
     * @return int|null
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int|null $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }

    /**
     * @return string
     */
    public function getContainerName()
    {
        return $this->containerName;
    }

    /**
     * @param string $containerName
     */
    public function setContainerName($containerName)
    {
        $this->containerName = $containerName;
    }

    /**
     * @return string
     */
    public function getBlNumber()
    {
        return $this->blNumber;
    }

    /**
     * @param string $blNumber
     */
    public function setBlNumber($blNumber)
    {
        $this->blNumber = $blNumber;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEts()
    {
        return $this->ets;
    }

    /**
     * @param \DateTimeInterface $ets
     */
    public function setEts($ets)
    {
        $this->ets = $ets;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEta()
    {
        return $this->eta;
    }

    /**
     * @param \DateTimeInterface $eta
     */
    public function setEta($eta)
    {
        $this->eta = $eta;
    }

    /**
     * @return PortEntity|Reference|null
     */
    public function getPol()
    {
        return $this->pol;
    }

    /**
     * @param PortEntity|Reference|null $pol
     */
    public function setPol($pol)
    {
        $this->pol = $pol;
    }

    /**
     * @return int|null
     */
    public function getPolId()
    {
        return $this->polId;
    }

    /**
     * @param int|null $polId
     */
    public function setPolId($polId)
    {
        $this->polId = $polId;
    }

    /**
     * @return PortEntity|Reference|null
     */
    public function getPod()
    {
        return $this->pod;
    }

    /**
     * @param PortEntity|Reference|null $pod
     */
    public function setPod($pod)
    {
        $this->pod = $pod;
    }

    /**
     * @return int|null
     */
    public function getPodId()
    {
        return $this->podId;
    }

    /**
     * @param int|null $podId
     */
    public function setPodId($podId)
    {
        $this->podId = $podId;
    }

    /**
     * @return ContainerTypeEntity|Reference|null
     */
    public function getContainerType()
    {
        return $this->containerType;
    }

    /**
     * @param ContainerTypeEntity|Reference|null $containerType
     */
    public function setContainerType($containerType)
    {
        $this->containerType = $containerType;
    }

    /**
     * @return int|null
     */
    public function getContainerTypeId()
    {
        return $this->containerTypeId;
    }

    /**
     * @param int|null $containerTypeId
     */
    public function setContainerTypeId($containerTypeId)
    {
        $this->containerTypeId = $containerTypeId;
    }

    /**
     * @return string
     */
    public function getSealNumber()
    {
        return $this->sealNumber;
    }

    /**
     * @param string $sealNumber
     */
    public function setSealNumber($sealNumber)
    {
        $this->sealNumber = $sealNumber;
    }

    /**
     * @return int|null
     */
    public function getResponsibleCsId()
    {
        return $this->responsibleCsId;
    }

    /**
     * @param int|null $responsibleCsId
     */
    public function setResponsibleCsId($responsibleCsId)
    {
        $this->responsibleCsId = $responsibleCsId;
    }

    /**
     * @return PortEntity|Reference|null
     */
    public function getShippingLine()
    {
        return $this->shippingLine;
    }

    /**
     * @param PortEntity|Reference|null $shippingLine
     */
    public function setShippingLine($shippingLine)
    {
        $this->shippingLine = $shippingLine;
    }

    /**
     * @return int|null
     */
    public function getShippingLineId(): ?int
    {
        return $this->shippingLineId;
    }

    /**
     * @param int|null $shippingLineId
     */
    public function setShippingLineId($shippingLineId)
    {
        $this->shippingLineId = $shippingLineId;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCarrierBookingDate()
    {
        return $this->carrierBookingDate;
    }

    /**
     * @param \DateTimeInterface $carrierBookingDate
     */
    public function setCarrierBookingDate($carrierBookingDate)
    {
        $this->carrierBookingDate = $carrierBookingDate;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getPcdEffective()
    {
        return $this->pcdEffective;
    }

    /**
     * @param \DateTimeInterface $pcdEffective
     */
    public function setPcdEffective($pcdEffective)
    {
        $this->pcdEffective = $pcdEffective;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getGateInDate()
    {
        return $this->gateInDate;
    }

    /**
     * @param \DateTimeInterface $gateInDate
     */
    public function setGateInDate($gateInDate)
    {
        $this->gateInDate = $gateInDate;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getLoadOnBoardDate()
    {
        return $this->loadOnBoardDate;
    }

    /**
     * @param \DateTimeInterface $loadOnBoardDate
     */
    public function setLoadOnBoardDate($loadOnBoardDate)
    {
        $this->loadOnBoardDate = $loadOnBoardDate;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEtaTrans()
    {
        return $this->etaTrans;
    }

    /**
     * @param \DateTimeInterface $etaTrans
     */
    public function setEtaTrans($etaTrans)
    {
        $this->etaTrans = $etaTrans;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getAtaTrans()
    {
        return $this->ataTrans;
    }

    /**
     * @param \DateTimeInterface $ataTrans
     */
    public function setAtaTrans($ataTrans)
    {
        $this->ataTrans = $ataTrans;
    }

    /**
     * @return ArrayCollection
     */
    public function getInvoiceLines()
    {
        return $this->invoiceLines;
    }

    public function setInvoiceLines($lines)
    {
        $this->invoiceLines = $lines;
    }

    /**
     * @return UserEntity|Reference|null
     */
    public function getResponsibleCs()
    {
        return $this->responsibleCs;
    }

    /**
     * @param UserEntity|Reference|null $responsibleCs
     */
    public function setResponsibleCs($responsibleCs)
    {
        $this->responsibleCs = $responsibleCs;
    }


}
