<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Relation\HasOne;
use Cycle\ORM\Promise\Reference;

/**
 * @Entity(repository="App\Repo\InvoiceLineRepo", table="invoice_lines")
 */
class InvoiceLineEntity
{
    /**
     * @var int|null
     *
     * @Column(type="bigPrimary", name="id")
     */
    private $invoiceLineId;

    /**
     * @var string
     *
     * @Column(type = "string(255)", name = "name")
     */
    private $name;

    /**
     * @var Reference|null|InvoiceEntity
     *
     * @HasOne(target = "App\Entity\InvoiceEntity", innerKey="invoiceId", outerKey="invoiceId")
     */
    private $invoice;

    /**
     * @var int
     *
     * @Column (type="bigInteger", name="invoice_id")
     */
    private $invoiceId;

    /**
     * @var Reference|null|ShipmentEntity
     *
     * @HasOne(target = "App\Entity\ShipmentEntity", innerKey="shipmentId", outerKey="shipmentId")
     */
    private $shipment;

    /**
     * @var int|null
     *
     * @Column(type="bigInteger", name="shipment_id")
     */
    private $shipmentId;
    /**
     * @var float
     *
     * @Column (type="decimal(11,2)", name="amount")
     */
    private $amount;

    /**
     * @return int|null
     */
    public function getInvoiceLineId()
    {
        return $this->invoiceLineId;
    }

    /**
     * @param int|null $invoiceLineId
     */
    public function setInvoiceLineId($invoiceLineId)
    {
        $this->invoiceLineId = $invoiceLineId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return InvoiceEntity|Reference|null
     */
    public function getInvoice(): Reference|InvoiceEntity|null
    {
        return $this->invoice;
    }

    /**
     * @param InvoiceEntity|Reference|null $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return ShipmentEntity|Reference|null
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * @param ShipmentEntity|Reference|null $shipment
     */
    public function setShipment($shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * @return int|null
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @param int|null $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
