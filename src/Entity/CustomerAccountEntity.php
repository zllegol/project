<?php

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Table;


/**
 * @Entity(repository="App\Repo\CustomerAccountRepo", table="customer_accounts")
 */
class CustomerAccountEntity
{
    /**
     * @var int|null
     *
     * @Column(type="bigPrimary", name="id")
     */
    private $customerAccountId;

    /**
     * @var string
     *
     * @Column(type = "string", name = "name")
     */
    private $name;

    /**
     * @return int|null
     */
    public function getCustomerAccountId()
    {
        return $this->customerAccountId;
    }

    /**
     * @param int|null $customerAccountId
     */
    public function setCustomerAccountId($customerAccountId)
    {
        $this->customerAccountId = $customerAccountId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
