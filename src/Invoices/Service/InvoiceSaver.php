<?php

namespace App\Invoices\Service;

use App\Domain\AppService;
use App\Entity\CustomerAccountEntity;
use App\Entity\CustomerEntity;
use App\Entity\InvoiceEntity;
use App\Repo\CustomerAccountRepo;
use Cycle\ORM\ORM;
use Cycle\ORM\Transaction;
use Doctrine\Common\Collections\Collection;
use Yiisoft\Validator\Result;
use Yiisoft\Validator\Rule\HasLength;
use Yiisoft\Validator\Rule\Number;
use Yiisoft\Validator\Rule\Required;
use Yiisoft\Validator\Rules;

class InvoiceSaver extends AppService
{
    private ORM $orm;

    /**
     * @param ORM $orm
     */
    public function __construct(ORM $orm)
    {
        $this->orm = $orm;
    }

    protected function job(Collection $options): mixed
    {
        $entity = $this->getEntity($options);

        $this->validateEntity($entity, $options);

        $entity->setName($options->get('name'));
        $entity->setCustomerId($options->get('customer_id'));
        $entity->setCustomerAccountId($options->get('customer_account_id'));
        $entity->setDocDate($options->get('doc_date'));
        $entity->setDueDate($options->get('due_date'));
        $entity->setAmount($options->get('amount'));
        $entity->setCurrency($options->get('currency'));
        $entity->setStatus($options->get('status'));
        $entity->setOutstandingAmount(0);

        $tr = new Transaction($this->orm);
        $tr->persist($entity);
        $tr->run();

        $res = [];
        $res['invoice_id'] = $entity->getInvoiceId();
        $res['name'] = $entity->getName();
        $res['customer_id'] = $entity->getCustomerId();
        $res['customer_account_id'] = $entity->getCustomerAccountId();
        $res['doc_date'] = $entity->getDocDate();
        $res['due_date'] = $entity->getDueDate();
        $res['amount'] = $entity->getAmount();
        $res['currency'] = $entity->getCurrency();
        $res['status'] = $entity->getStatus();

        return $res;
    }

    private function validateEntity(InvoiceEntity $entity, Collection $options)
    {
        $nameRules = new Rules([
            Required::rule(),
            HasLength::rule()->min(2)->max(100)
        ]);

        $this->throwErrorMessageIfNeeded($nameRules->validate($options->get('name')), 'name');

        $numberRules = new Rules([
            Required::rule(),
            Number::rule()
        ]);

        $this->throwErrorMessageIfNeeded($numberRules->validate($options->get('customer_account_id')), 'customer_account_id');
        /** @var CustomerAccountRepo $customerAccountRepo */
        $customerAccountRepo = $this->orm->getRepository(CustomerAccountEntity::class);
        if (!$customerAccountRepo->findByPK($options->get('customer_account_id'))) {
            throw new \InvalidArgumentException('Invalid customer account');
        }

        $this->throwErrorMessageIfNeeded($numberRules->validate($options->get('customer_id')), 'customer_id');
        $customerRepo = $this->orm->getRepository(CustomerEntity::class);
        if (!$customerRepo->findByPK($options->get('customer_id'))) {
            throw new \InvalidArgumentException('Invalid customer');
        }

        if (!date_create($options->get('doc_date'))) {
            throw new \InvalidArgumentException('Invalid doc_date');
        }

        if (!date_create($options->get('due_date'))) {
            throw new \InvalidArgumentException('Invalid doc_date');
        }

        $this->throwErrorMessageIfNeeded($numberRules->validate($options->get('amount')), 'amount');

        $currencyRules = new Rules([
            Required::rule(),
            HasLength::rule()->min(3)->max(3)
        ]);

        $this->throwErrorMessageIfNeeded($currencyRules->validate($options->get('currency')), 'currency');

        if (!in_array($options->get('status'), [11, 22])) {
            throw new \InvalidArgumentException('Invalid status');
        }
    }

    private function throwErrorMessageIfNeeded(Result $result, $key)
    {
        if (!$result->isValid()) {
            throw new \InvalidArgumentException($key . ': ' . implode(PHP_EOL, $result->getErrors()));
        }
    }

    /**
     * @param Collection $options
     * @return InvoiceEntity|object
     */
    private function getEntity(Collection $options): InvoiceEntity
    {
        $entity = null;
        if ($options->get('invoice_id')) {
            $entity = $this->orm->getRepository(InvoiceEntity::class)->findByPK($options->get('invoice_id'));
        }

        if (!$entity) {
            $entity = new InvoiceEntity();
        }

        return $entity;
    }
}
