<?php

namespace App\Invoices\Service;

use App\Domain\AbstractProvider;
use App\Entity\InvoiceEntity;
use App\Entity\InvoiceLineEntity;
use App\Repo\InvoiceLineRepo;
use App\Repo\InvoiceRepo;
use Cycle\ORM\Select\QueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Yiisoft\Yii\Cycle\Data\Reader\EntityReader;

class InvoiceProvider extends AbstractProvider
{
    protected function filterOptions(bool $withInitOptions = true): array
    {
        $filterData = $this->session->get($this->getSessionNamespace(), []);

        $res = [
            'name' => $filterData['name'] ?? '',
            'doc_date_from' => $filterData['doc_date_from'] ?? '',
            'doc_date_to' => $filterData['doc_date_to'] ?? '',
            'due_date_from' => $filterData['due_date_from'] ?? '',
            'due_date_to' => $filterData['due_date_to'] ?? '',
            'status' => $filterData['status'] ?? '',
        ];

        if ($withInitOptions) {
            $res['status_select_options'] = [
                11 => 'Не оплачен',
                22 => 'Оплачен'
            ];
        }

        return $res;
    }

    protected function grid(Collection $options): array
    {
        $filterOptions = array_filter($this->filterOptions(false));

        /** @var InvoiceRepo $invoiceRepo */
        $invoiceRepo = $this->orm->getRepository(InvoiceEntity::class);

        $select = $invoiceRepo->select();

        if (isset($filterOptions['name'])) {
            $select->andWhere('name', 'LIKE', '%' . $filterOptions['name'] . '%');
        }

        if (isset($filterOptions['status'])) {
            $select->andWhere('status', '=', $filterOptions['status']);
        }

        if (isset($filterOptions['doc_date_from']) && isset($filterOptions['doc_date_to'])) {
            $select->andWhere(function (QueryBuilder $select) use ($filterOptions) {
                $select->where('doc_date', '>=', $filterOptions['doc_date_from'])->andWhere('doc_date', '<=', $filterOptions['doc_date_to']);
            });
        } elseif (isset($filterOptions['doc_date_from'])) {
            $select->andWhere('doc_date', '>=', $filterOptions['doc_date_from']);
        } elseif (isset($filterOptions['doc_date_to'])) {
            $select->andWhere('doc_date', '<=', $filterOptions['doc_date_to']);
        }

        if (isset($filterOptions['due_date_from']) && isset($filterOptions['due_date_to'])) {
            $select->andWhere(function (QueryBuilder $select) use ($filterOptions) {
                $select->where('due_date', '>=', $filterOptions['due_date_from'])->andWhere('due_date', '<=', $filterOptions['due_date_to']);
            });
        } elseif (isset($filterOptions['due_date_from'])) {
            $select->andWhere('due_date', '>=', $filterOptions['due_date_from']);
        } elseif (isset($filterOptions['due_date_to'])) {
            $select->andWhere('due_date', '<=', $filterOptions['due_date_to']);
        }

        $reader = new EntityReader($select);

        $page = $this->getCurrentPageNumber($options);

        $paginator = $this->createPaginator($reader, $page);

        return ['paginator' => $paginator, 'page' => $page, 'page_count' => $paginator->getTotalPages()];
    }

    protected function detail(Collection $options): mixed
    {
        $id = $options->get('id');

        if (!$id) {
            throw new \UnexpectedValueException('Id is missing');
        }

        /** @var InvoiceRepo $invoiceRepo */
        $invoiceRepo = $this->orm->getRepository(InvoiceEntity::class);
        /** @var InvoiceEntity $item */
        $item = $invoiceRepo->select()->wherePK($id)->load(['customerAccount', 'customer', 'invoiceLines'])->fetchOne();

        if (!$item) {
            throw new \InvalidArgumentException('Invalid id passed');
        }

        /** @var InvoiceLineRepo $invoiceLineRepo */
        $invoiceLineRepo = $this->orm->getRepository(InvoiceLineEntity::class);

        $invoiceLines = $invoiceLineRepo->select()->where('invoice_id', '=', $item->getInvoiceId())->load('shipment')->fetchAll();

        $item->setInvoiceLines(new ArrayCollection($invoiceLines));

        return $item;
    }

    protected function getSessionNamespace(): string
    {
        return __NAMESPACE__ . __CLASS__;
    }

}
