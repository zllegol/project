<?php

namespace App\Invoices\Form;

use Yiisoft\Form\FormModel;

class InvoicesFilterForm extends FormModel
{
    private string $name = '';
    private string $status = '';
    private string $doc_date_from = '';
    private string $doc_date_to = '';
    private string $due_date_from = '';
    private string $due_date_to = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getDocDateFrom(): string
    {
        return $this->doc_date_from;
    }

    /**
     * @return string
     */
    public function getDocDateTo(): string
    {
        return $this->doc_date_to;
    }

    /**
     * @return string
     */
    public function getDueDateFrom(): string
    {
        return $this->due_date_from;
    }

    /**
     * @return string
     */
    public function getDueDateTo(): string
    {
        return $this->due_date_to;
    }

    public function getAttributeLabels(): array
    {
        return [
            'name' => 'Счет №',
            'doc_date_from' => 'Дата документа',
            'doc_date_to' => '',
            'due_date_from' => 'Срок оплаты',
            'due_date_to' => '',
            'status' => 'Статус'
        ];
    }


}
