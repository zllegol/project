<?php

namespace App\Invoices\Controller;

use App\Invoices\Form\InvoicesFilterForm;
use App\Invoices\Service\InvoiceProvider;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Yiisoft\Router\UrlGeneratorInterface;
use Yiisoft\Yii\View\ViewRenderer;

class InvoiceController
{
    private ResponseFactoryInterface $responseFactory;
    private ViewRenderer $viewRenderer;
    private UrlGeneratorInterface $urlGenerator;
    private InvoiceProvider $invoiceProvider;

    public function __construct(ResponseFactoryInterface $responseFactory, ViewRenderer $viewRenderer, UrlGeneratorInterface $urlGenerator, InvoiceProvider $invoiceProvider)
    {
        $this->responseFactory = $responseFactory;
        $this->viewRenderer = $viewRenderer->withControllerName('invoice');
        $this->urlGenerator = $urlGenerator;
        $this->invoiceProvider = $invoiceProvider;
    }

    public function indexAction(ServerRequestInterface $request): ResponseInterface
    {
        $filterForm = new InvoicesFilterForm();

        if ($request->getMethod() == 'POST') {
            $params = $request->getParsedBody();

            if (isset($params[$filterForm->getFormName()])) {
                $this->invoiceProvider->serve(['filter' => true, 'filter_data' => $params[$filterForm->getFormName()]]);
            }

            return $this->responseFactory
                ->createResponse(302)
                ->withHeader('Location', $this->urlGenerator->generate('invoices'));
        }

        if (isset($request->getQueryParams()['filter_clear'])) {
            $this->invoiceProvider->serve(['filter_clear' => true]);
            return $this->responseFactory
                ->createResponse(302)
                ->withHeader('Location', $this->urlGenerator->generate('invoices'));
        }

        $result = $this->invoiceProvider->serve(['filter_options' => true]);
        $result->thrErrIfExist();
        $filterOptions = $result->getResult();

        $result = $this->invoiceProvider->serve(['grid' => true, 'request_data' => $request->getQueryParams()]);
        $result->thrErrIfExist();
        $gridData = $result->getResult();

        return $this->viewRenderer->render('index', [
            'filterForm' => $filterForm,
            'gridData' => $gridData,
            'filterOptions' => $filterOptions
        ]);
    }

    public function detailAction(ServerRequestInterface $request): ResponseInterface
    {
        $res = $this->invoiceProvider->serve(['detail' => true] + $request->getAttributes());

        $res->thrErrIfExist();

        return $this->viewRenderer->render('detail', ['item' => $res->getResult()]);
    }
}
