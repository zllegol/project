<?php

namespace App\Invoices\Controller\Api;

use App\Invoices\Service\InvoiceSaver;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class InvoiceController
{
    private ResponseFactoryInterface $responseFactory;

    private InvoiceSaver $invoiceSaver;

    /**
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(ResponseFactoryInterface $responseFactory, InvoiceSaver $invoiceSaver)
    {
        $this->responseFactory = $responseFactory;
        $this->invoiceSaver = $invoiceSaver;
    }

    public function postAction(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $body = json_decode(trim($request->getBody()->getContents()), true);
            if (!$body) {
                throw new \Error();
            }
        } catch (\Throwable $ex) {
            return $this->responseFactory->createResponse(400);
        }

        $result = $this->invoiceSaver->serve($body);

        $error = '';
        try {
            $result->thrErrIfExist();
        } catch (\InvalidArgumentException $ex) {
            $error = $ex->getMessage();
        } catch (\Throwable $ex) {
            $error = 'Internal error ' . $ex->getMessage();
        }

        $data = ['status' => true];

        if ($error) {
            $data['status'] = false;
            $data['msg'] = $error;
        } else {
            $data['data'] = $result->getResult();
        }

        $response = $this->responseFactory->createResponse($error ? 400 : 201);
        $response = $response->withHeader('Content-Type', 'application/json');
        $response->getBody()->write(json_encode($data));
        return $response;
    }
}
