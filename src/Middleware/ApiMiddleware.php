<?php

namespace App\Middleware;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Yiisoft\Router\UrlMatcherInterface;

class ApiMiddleware implements MiddlewareInterface
{
    private ResponseFactoryInterface $responseFactory;

    /**
     * @param UrlMatcherInterface $matcher
     */
    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authentication = $request->getHeader('Authorization');
        $authentication = implode('', $authentication);
        if ($authentication != getenv('API_TOKEN')) {
            return $this->responseFactory->createResponse(401);
        }
        return $handler->handle($request);
    }
}
