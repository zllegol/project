<?php

declare(strict_types=1);

use App\ViewInjection\ContentViewInjection;
use App\ViewInjection\LayoutViewInjection;
use Yiisoft\Definitions\Reference;
use Yiisoft\Yii\View\CsrfViewInjection;

return [
    'yiisoft/yii-view' => [
        'viewPath' => '@views',
        'layout' => '@layout/main',
        'injections' => [
            Reference::to(ContentViewInjection::class),
            Reference::to(CsrfViewInjection::class),
            Reference::to(LayoutViewInjection::class),
            // Use for add Csrf parameter to all views
            // Reference::to(CsrfViewInjection::class),
            // or
            // DynamicReference::to(function (ContainerInterface $container) {
            //     return $container->get(CsrfViewInjection::class)->withParameter('mycsrf');
            // }),
        ],
    ],
];
