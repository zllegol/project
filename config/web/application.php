<?php

declare(strict_types=1);

use App\Handler\NotFoundHandler;
use App\Middleware\ApiMiddleware;
use App\User\Middleware\UserMiddleware;
use Cycle\ORM\ORMInterface;
use Psr\Container\ContainerInterface;
use Yiisoft\Auth\IdentityRepositoryInterface;
use Yiisoft\Csrf\CsrfMiddleware;
use Yiisoft\ErrorHandler\Middleware\ErrorCatcher;
use Yiisoft\Definitions\Reference;
use Yiisoft\Definitions\DynamicReference;
use Yiisoft\Injector\Injector;
use Yiisoft\Middleware\Dispatcher\MiddlewareDispatcher;
use Yiisoft\Router\Middleware\Router;
use Yiisoft\Session\SessionMiddleware;
use Yiisoft\Yii\Web\ServerRequestFactory;

return [
    Yiisoft\Yii\Web\Application::class => [
        '__construct()' => [
            'dispatcher' => DynamicReference::to(static function (Injector $injector) {
                $middlewareStack = [Router::class];

                if (str_starts_with($_SERVER['REQUEST_URI'], '/api')) {
                    $middlewareStack = array_merge($middlewareStack, [ApiMiddleware::class]);
                } else {
                    $middlewareStack = array_merge($middlewareStack, [UserMiddleware::class, CsrfMiddleware::class]);
                }

                $middlewareStack = array_merge($middlewareStack, [SessionMiddleware::class, ErrorCatcher::class]);

                return ($injector->make(MiddlewareDispatcher::class))
                    ->withMiddlewares($middlewareStack);
            }),
            'fallbackHandler' => Reference::to(NotFoundHandler::class),
        ],
    ],
    IdentityRepositoryInterface::class => static function (ContainerInterface $container) {
        return $container->get(ORMInterface::class)->getRepository(\App\Entity\UserEntity::class);
    },
    \App\User\Command\CreateUserCommand::class => static function (ContainerInterface $container) {
        return new \App\User\Command\CreateUserCommand($container->get(ORMInterface::class));
    },
];
