<?php

declare(strict_types=1);

use Yiisoft\Config\Config;
use Yiisoft\DataResponse\Middleware\FormatDataResponse;
use Yiisoft\Csrf\CsrfMiddleware;
use Yiisoft\Router\Group;
use Yiisoft\Router\RouteCollection;
use Yiisoft\Router\RouteCollectionInterface;
use Yiisoft\Router\RouteCollectorInterface;

/** @var Config $config */

return [
    RouteCollectionInterface::class => static function (RouteCollectorInterface $collector) use ($config) {
        if (!str_starts_with($_SERVER['REQUEST_URI'], '/api')) {
            $collector
                ->middleware(CsrfMiddleware::class);
        }

        $collector
            ->middleware(FormatDataResponse::class)
            ->addGroup(
                Group::create(null)
                    ->routes(...$config->get('routes'))
            );

        return new RouteCollection($collector);
    },
];
