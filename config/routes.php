<?php

declare(strict_types=1);

use App\Controller\SiteController;
use App\Invoices\Controller\InvoiceController;
use App\Shipment\Controller\ShipmentController;
use Yiisoft\Http\Method;
use Yiisoft\Router\Route;

return [
    Route::get('/')->action([SiteController::class, 'index'])->name('home'),
    Route::methods([Method::GET, Method::POST], '/login')->action([\App\User\Controller\UserController::class, 'loginAction'])->name('login'),
    Route::methods([Method::GET], '/logout')->action([\App\User\Controller\UserController::class, 'logoutAction'])->name('logout'),
    Route::methods([Method::GET, Method::POST], '/shipments')->action([ShipmentController::class, 'indexAction'])->name('shipments'),
    Route::get('/shipments/detail/{id:\d+}')->action([ShipmentController::class, 'detailAction'])->name('shipments.detail'),
    Route::methods([Method::GET, Method::POST], '/invoices')->action([InvoiceController::class, 'indexAction'])->name('invoices'),
    Route::get('/invoices/detail/{id:\d+}')->action([InvoiceController::class, 'detailAction'])->name('invoices.detail'),


    // API
    Route::post('/api/shipments')->action([\App\Shipment\Controller\Api\ShipmentController::class, 'postAction'])->name('api.shipments'),
    Route::post('/api/invoices')->action([\App\Invoices\Controller\Api\InvoiceController::class, 'postAction'])->name('api.invoices')
];
